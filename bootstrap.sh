#!/bin/sh

# Downloading Kafka and Zookeeper
wget -nc ftp://mirrors.ircam.fr/pub/apache/kafka/1.1.0/kafka_2.11-1.1.0.tgz

# Extracting archive
tar -xf kafka_2.11-1.1.0.tgz

# Setting up ScyllaDB
cqlsh < database.cql