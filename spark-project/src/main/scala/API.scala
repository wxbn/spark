import cats.effect._, org.http4s._, org.http4s.dsl.io._, scala.concurrent.ExecutionContext.Implicits.global
import org.http4s.client

import fs2.{Stream, StreamApp}
import fs2.StreamApp.ExitCode
import org.http4s.server.blaze._

import java.util.Properties
import java.time.Instant
import org.apache.kafka.clients.producer._

import play.api.libs.json.{JsError, JsSuccess, Json}

import misc.PostUtils._
import misc.ProfileUtils._
import misc.MessageUtils._
import SearchTool._


object API extends StreamApp[IO] {

    val  props = new Properties()
    props.put("bootstrap.servers", "localhost:9092")

    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

    val producer = new KafkaProducer[String, String](props)

    val service = HttpService[IO] {
        case req @ POST -> Root / "addPost" => Ok(req.bodyAsText.filter(x => sendPost(x)).map(x => "Success"))
        case req @ POST -> Root / "addProfile" => Ok(req.bodyAsText.filter(x => sendProfile(x)).map(x => "Success"))
        case req @ POST -> Root / "addMessage" => Ok(req.bodyAsText.filter(x => sendMessage(x)).map(x => "Success"))
        case req @ POST -> Root / "search" => Ok(req.bodyAsText.filter(x => launchsearch(x)).map(x => "Success"))
    }

    def sendPost(x: String) : Boolean = {
        StringToPost(x) match {
            case Some(y) => producer.send(new ProducerRecord("socialNetworkContent", "posts", PostToString(y))); true
            case None => false
        }
    }

    def sendProfile(x: String) : Boolean = {
        StringToProfile(x) match {
            case Some(y) => producer.send(new ProducerRecord("socialNetworkContent", "profiles", ProfileToString(y))); true
            case None => false
        }
    }

    def sendMessage(x: String) : Boolean = {
        StringToMessage(x) match {
            case Some(y) => producer.send(new ProducerRecord("socialNetworkContent", "messages", MessageToString(y))); true
            case None => false
        }
    }

    def launchsearch(x: String) : Boolean = {
        StringToSearch(x) match {
            case Some(s) => searchMessages(s.keywords, s.conditional,Some(s.from), Some(s.to));
                            searchPosts(s.keywords, s.conditional,Some(s.from), Some(s.to)); true
            case None => false
        }
    }

    case class Search (
		keywords : List[String],
        conditional: String,
        from: Long,
        to: Long
	)

	implicit val searchFormat = Json.format[Search]

	def StringToSearch(str : String) = Json.parse(str).validate[Search] match {
		case JsError(e) => println(e); None
		case JsSuccess(t, _) => Some(t)
	}

    override def stream(args: List[String], requestShutdown: IO[Unit]): Stream[IO, ExitCode] =
        BlazeBuilder[IO]
            .bindHttp(8080, "localhost")
            .mountService(service, "/")
            .serve
}