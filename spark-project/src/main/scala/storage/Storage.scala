package storage

import org.apache.kafka.clients.consumer._
import org.apache.spark.{SparkContext,SparkConf}
import com.datastax.spark.connector._
import com.datastax.spark.connector.streaming._
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.streaming.StreamingContext._
import org.apache.spark.streaming.dstream.ConstantInputDStream


import misc.PostUtils._
import misc.ProfileUtils._
import misc.MessageUtils._

object Storage {
    def cache(rec: ConsumerRecord[String,String]) : Unit = {
        rec.key match {
            case "posts" => storeToDB(rec.key, StringToPost(rec.value))
            case "profiles" => storeToDB(rec.key, StringToProfile(rec.value))
            case "messages" => storeToDB(rec.key, StringToMessage(rec.value))
            case _ => println("Error in ConsumerRecord")
        }
    }

    def storeToDB(table: String, obj: Option[Any]) {
        val conf = new SparkConf()
            .setAppName("ScyllaDB_cache")
            .setMaster("local[*]")

        val sc = SparkContext.getOrCreate(conf)

        obj match {
            case Some(obj) => obj match {
                                            case post : Post => sc.parallelize(Seq(post))
                                                                .saveToCassandra("social_network", table)
                                            case profile : Profile => sc.parallelize(Seq(profile))
                                                                .saveToCassandra("social_network", table)
                                            case message : Message => sc.parallelize(Seq(message))
                                                                .saveToCassandra("social_network", table)
                                        }
            case None => println("Error in ConsumerRecord message")
        }
    }

    def persistToHDFS() {
        val conf = new SparkConf()
            .setAppName("HDFS_persistance")
            .setMaster("local[*]")

        val sc = new SparkContext(conf)
        val ssc = new StreamingContext(sc, Seconds(10))

        writeToHDFS(ssc, "posts")
        writeToHDFS(ssc, "profiles")
        writeToHDFS(ssc, "messages")

        ssc.start()
    }

    def writeToHDFS(ssc: StreamingContext, key: String) {
        val cassandraRDD = ssc.cassandraTable("social_network", key)
        val dstream = new ConstantInputDStream(ssc, cassandraRDD)

        dstream.foreachRDD { rdd =>
            rdd.map(rowToJson(_, key))
            .filter(_.length > 0)
            .saveAsTextFile("/tmp/hdfs/" + key)
        }
    }

    def rowToJson(row: CassandraRow, key: String) : String = {
        key match {
            case "posts" => PostToString(Post(row.getInt("id"),
                                row.getString("sender"), row.getString("place"),
                                row.getString("title"), row.getString("body"),
                                row.getLong("time")))
            case "profiles" => ProfileToString(Profile(row.getString("name"),
                                row.getString("username"), row.getString("picture")))
            case "messages" => MessageToString(Message(row.getInt("id"),
                                row.getString("sender"), row.getString("recipient"),
                                row.getString("body"), row.getLong("time")))
            case _ => ""
        }
    }
}