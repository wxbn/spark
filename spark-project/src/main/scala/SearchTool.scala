import org.apache.spark.{SparkContext,SparkConf}
import com.datastax.spark.connector._
import com.datastax.spark.connector.streaming._
import java.text.SimpleDateFormat
import scala.util.{Try,Success,Failure}

import misc.MessageUtils._
import misc.PostUtils._

object SearchTool {

    def typeKeywords() : Unit = {
        println("Which brands do you want to search ?")
        println("[search] to stop adding brands")
        val list = getBrands(List())
        val cond = getCond()
        val date_before = Some(getDate(0.toLong))
        val date_after = Some(getDate(Long.MaxValue))
        searchMessages(list, cond, date_before, date_after)
        searchPosts(list, cond, date_before, date_after)
    }

    def getBrands(l : List[String]) : List[String] = {
        scala.io.StdIn.readLine() match {
        case "search" => l
        case e => getBrands(e::l)
        }
    }

    def getCond() : String = {
      println("Do you want to use the operator AND or OR with the selected brands")
      val res = scala.io.StdIn.readLine()
      if (res == "OR" || res == "AND") {
        res
      } else {
        getCond()
      }
    }

    def getDate(default : Long) : Long = {
      val format = new SimpleDateFormat("dd-MM-yyyy")
      println("Type a limit date with the format dd-MM-yyyy")
      val date_str = scala.io.StdIn.readLine()
      val date_date : Long = Try({
        format.parse(date_str).toInstant().toEpochMilli()
      }).recoverWith({
        case (ex : Throwable) => ex.printStackTrace; Failure(ex);
        }).getOrElse(default)
      if (date_date != default) {
        date_date
      } else {
        getDate(default)
      }
    }

    def searchMessages(keywords: List[String], conditional: String, before: Option[Long], after: Option[Long]) {
        val conf = new SparkConf()
                .setAppName("searchTool")
                .setMaster("local[*]")

        val sc = new SparkContext(conf)

        val rdd = sc.textFile("/tmp/hdfs/messages")
          .map(StringToMessage(_))
          .filter(_ != None)

        val timeFilteredRDD = rdd.filter(d => d.get.time >= before.getOrElse(0.toLong)
                                    && d.get.time <= after.getOrElse(Long.MaxValue))

        val filteredRDD = conditional match {
            case "OR" => timeFilteredRDD.filter(data => OR(data.get.body, keywords))
            case "AND" => timeFilteredRDD.filter(data => AND(data.get.body, keywords))
        }

        if (filteredRDD.isEmpty()) {
          println("No results found.")
        } else {
          filteredRDD.foreach(println)
        }
        
        sc.stop()
    }

    def searchPosts(keywords: List[String], conditional: String, before: Option[Long], after: Option[Long]) {
        val conf = new SparkConf()
                .setAppName("searchTool")
                .setMaster("local[*]")

        val sc = new SparkContext(conf)

        val rdd = sc.textFile("/tmp/hdfs/posts")
          .map(StringToPost(_))
          .filter(_ != None)

        val timeFilteredRDD = rdd.filter(d => d.get.time >= before.getOrElse(0.toLong)
                                    && d.get.time <= after.getOrElse(Long.MaxValue))

        val filteredRDD = conditional match {
            case "OR" => timeFilteredRDD.filter(data => OR(data.get.body, keywords))
            case "AND" => timeFilteredRDD.filter(data => AND(data.get.body, keywords))
        }

        if (filteredRDD.isEmpty()) {
          println("No results found.")
        } else {
          filteredRDD.foreach(println)
        }
        
        sc.stop()
    }

    def OR(body: String, keywords: List[String]) : Boolean = {
        keywords.filter(k => body contains k).length > 0
    }

    def AND(body: String, keywords: List[String]) : Boolean = {
        keywords.filter(k => body contains k).length == keywords.length
    }
}
