package misc

import play.api.libs.json.{JsError, JsSuccess, Json}
import java.sql.Timestamp

object PostUtils {
	case class Post (
		id : Int,
		sender : String,
		place : String,
		title : String,
    	body : String,
    	time : Long
	)

	implicit val postFormat = Json.format[Post]

	def StringToPost(str : String) = Json.parse(str).validate[Post] match {
		case JsError(e) => println(e); None
		case JsSuccess(t, _) => Some(t)
	}

    def PostToString(post : Post) = Json.toJson(post).toString
}

object ProfileUtils {
	case class Profile (
		name : String,
		username : String,
    	picture : String
	)

	implicit val profileFormat = Json.format[Profile]

	def StringToProfile(str : String) = Json.parse(str).validate[Profile] match {
		case JsError(e) => println(e); None
		case JsSuccess(t, _) => Some(t)
	}

    def ProfileToString(profile : Profile) = Json.toJson(profile).toString
}
	
object MessageUtils {
	case class Message (
		id : Int,
		sender : String,
		recipient : String,
		body : String,
    	time : Long
	)

	implicit val messageFormat = Json.format[Message]

	def StringToMessage(str : String) = Json.parse(str).validate[Message] match {
		case JsError(e) => println(e); None
		case JsSuccess(t, _) => Some(t)
	}

    def MessageToString(message : Message) = Json.toJson(message).toString
}
