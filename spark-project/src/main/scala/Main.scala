object Main {
  def main(args: Array[String]): Unit = {

    println("Choose how to launch the application :")
    println("[P] as producer")
    println("[C] as consumer")
    println("[S] as search tool")
    println("[A] as REST API")

    scala.io.StdIn.readLine() match {
        case "C" => kafka.Consumer.listen()
        case "c" => kafka.Consumer.listen()
        case "S" => SearchTool.typeKeywords()
        case "s" => SearchTool.typeKeywords()
        case "A" => API.main(Array.empty)
        case "a" => API.main(Array.empty)
        case _ => kafka.Producer.produce()
    }
  }
}
