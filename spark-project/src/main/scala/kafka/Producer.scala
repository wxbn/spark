package kafka

import java.util.Properties
import java.time.Instant
import org.apache.kafka.clients.producer._

import misc.MessageUtils._
import misc.ProfileUtils._
import misc.PostUtils._

object Producer {
    val  props = new Properties()
    props.put("bootstrap.servers", "localhost:9092")

    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

    val producer = new KafkaProducer[String, String](props)

    println("Producer is ready")
    
    def produce() : Unit = {
        println("Choose the type of topic :")
        println("[m] for message")
        println("[p] for post")
        println("[u] for profile")
        
        scala.io.StdIn.readLine() match {
          case "m" =>  sendMessage()
          case "M" =>  sendMessage()
          case "p" =>  sendPost()
          case "P" =>  sendPost()
          case "u" =>  sendProfile()
          case "U" =>  sendProfile()
          case _   =>  produce()
        }
    }

    def sendMessage() : Unit = {
      val id = askArg("id").toInt
      val sender = askArg("sender")
      val recipient = askArg("recipient")
      val body = askArg("body")
      val time = Instant.now().toEpochMilli()
      val m = Message(id, sender, recipient, body, time)
      producer.send(new ProducerRecord("socialNetworkContent", "messages", MessageToString(m)))
      producer.close()
      println("Message sent")
    }

    def sendPost() : Unit = {
      val id = askArg("id").toInt
      val sender = askArg("sender")
      val place = askArg("place")
      val title = askArg("title")
      val body = askArg("body")
      val time = Instant.now().toEpochMilli()
      val post = Post(id, sender, place, title, body, time)
      producer.send(new ProducerRecord("socialNetworkContent", "posts", PostToString(post)))
      producer.close()
      println("Message sent")
    }

    def sendProfile() : Unit = {
      val name = askArg("name")
      val username = askArg("username")
      val picture = askArg("picture")
      val profile = Profile(name, username, picture)
      producer.send(new ProducerRecord("socialNetworkContent", "profiles", ProfileToString(profile)))
      producer.close()
      println("Message sent")
    }

    def askArg(arg: String) : String = {
      println("Choose the " + arg)
      val res = scala.io.StdIn.readLine()
      res
    }
}
