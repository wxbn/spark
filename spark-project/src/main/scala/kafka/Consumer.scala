package kafka

import java.util
import org.apache.kafka.clients.consumer._
import scala.collection.JavaConverters._
import java.util.Properties
import storage.Storage._

object Consumer {
    val  props = new Properties()
    props.put("bootstrap.servers", "localhost:9092")

    props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    props.put("group.id", "something")

    val consumer = new KafkaConsumer[String, String](props)
    consumer.subscribe(util.Collections.singletonList("socialNetworkContent"))

    def listen() : Unit = {
        persistToHDFS; consume
    }

    def consume() : Unit = {
        while(true) {
            consumer.poll(100).asScala
            .foreach(cache(_))
        }
    }
}