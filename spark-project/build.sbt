name := "Spark Project"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies += "org.apache.kafka" %% "kafka" % "1.1.0"

libraryDependencies += "org.apache.spark" %% "spark-core" % "2.3.1"

libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.3.1"
libraryDependencies += "org.apache.spark" %% "spark-catalyst" % "2.3.1" % "provided"
libraryDependencies += "com.datastax.spark" %% "spark-cassandra-connector" % "2.3.0"

libraryDependencies += "org.apache.spark" %% "spark-streaming" % "2.3.1"

dependencyOverrides += "com.fasterxml.jackson.core" % "jackson-core" % "2.6.5"
dependencyOverrides += "com.fasterxml.jackson.core" % "jackson-databind" % "2.6.5"
libraryDependencies += "com.typesafe.play" %% "play-json" % "2.6.7"

libraryDependencies += "org.http4s" %% "http4s-dsl" % "0.18.10"
libraryDependencies += "org.http4s" %% "http4s-blaze-server" % "0.18.10"
libraryDependencies += "org.http4s" %% "http4s-blaze-client" % "0.18.10"

libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.6"

mainClass in (Compile, run) := Some("Main")
mainClass in assembly := Some("Main")

assemblyMergeStrategy in assembly := {
 case PathList("META-INF", xs @ _*) => MergeStrategy.discard
 case x => MergeStrategy.first
}