#!/bin/sh

cd kafka_2.11-1.1.0

# Stoping previous Kafka instances
bin/kafka-server-stop.sh > /dev/null

# Stoping previous Zookeeper instances
bin/zookeeper-server-stop.sh > /dev/null

# Starting Zookeeper
bin/zookeeper-server-start.sh config/zookeeper.properties > /dev/null &

# Starting Kafka
bin/kafka-server-start.sh config/server.properties