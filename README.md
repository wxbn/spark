# Spark-Kafka Project

### To download and start Zookeeper and Kafka cluster :
```sh
$ sh bootstrap.sh
$ sh start_kafka_server.sh
```

### To run the project :
```sh
$ cd spark-project
$ sbt
$ > ~run
```

### To run the project on docker :
```sh
$ docker pull wxbn/generic-jvm
$ docker run -it wxbn/generic-jvm /usr/lib/jvm/java-1.8-openjdk/bin/java -jar project.jar
```